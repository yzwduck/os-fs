#include <string.h>
#include <stdio.h>
#include "file.h"
#include "inode.h"
#include "fs.h"

static uint32_t efs_calc_abs_data_blk(struct efs_sb_info *ctx,
                                      struct efs_inode *inode,
                                      uint32_t inode_id, uint32_t rel_blk) {
  uint32_t offsets[4];
  uint32_t abs_blk[4];
  int i;
  int n = efs_block_to_path(&ctx->inodes, rel_blk, offsets);
  abs_blk[0] = inode->i_block[offsets[0]] + ctx->bm_block.data_offset;

  for (i = 1; i < n; i++) {
    uint32_t *blks;
    efs_cache_fetch(ctx->bm_block.cache, abs_blk[i - 1], &blks);
    abs_blk[i] = blks[offsets[i]] + ctx->bm_block.data_offset;
    efs_cache_release(ctx->bm_block.cache, abs_blk[i - 1]);
  }

  return abs_blk[n - 1];
}

int efs_file_creat(struct efs_sb_info *ctx, struct efs_inode *inode,
                   struct efs_file *f) {
  uint32_t inode_id = efs_bitmap_next(&ctx->bm_inode, 0);
  if (inode_id == -1) return -1;
  efs_bitmap_set(&ctx->bm_inode, inode_id);
  struct efs_inode *inode_block;
  const uint32_t inodes_per_block =
      ctx->bm_inode.blk_size / sizeof(struct efs_inode);
  const uint32_t abs_blk =
      ctx->bm_inode.data_offset + inode_id / inodes_per_block;
  const uint32_t inode_index = inode_id % inodes_per_block;

  memset(f, 0, sizeof(*f));
  f->inode_id = inode_id;
  f->last_block = -1;
  f->last_abs_blk = -1;

  efs_cache_fetch(&ctx->cache, abs_blk, &inode_block);
  efs_cache_dirty(&ctx->cache, abs_blk);
  f->inode = &inode_block[inode_index];
  memcpy(f->inode, inode, sizeof(*inode));
  return 0;
}

int efs_file_delete(struct efs_sb_info *ctx, uint32_t inode_id) {
  struct efs_file f;
  efs_file_open(ctx, inode_id, &f);
  efs_file_trunc(ctx, &f, 0);
  efs_file_close(ctx, &f);
  efs_bitmap_clr(&ctx->bm_inode, inode_id);
  return 0;
}

int efs_file_open(struct efs_sb_info *ctx, uint32_t inode_id,
                  struct efs_file *f) {
  struct efs_inode *inode_block;
  const uint32_t inodes_per_block =
      ctx->bm_inode.blk_size / sizeof(struct efs_inode);
  const uint32_t abs_blk =
      ctx->bm_inode.data_offset + inode_id / inodes_per_block;
  const uint32_t inode_index = inode_id % inodes_per_block;

  memset(f, 0, sizeof(*f));
  f->inode_id = inode_id;
  f->last_block = -1;
  f->last_abs_blk = -1;

  efs_cache_fetch(&ctx->cache, abs_blk, &inode_block);
  efs_cache_dirty(&ctx->cache, abs_blk);
  f->inode = &inode_block[inode_index];
  return 0;
}

int efs_file_close(struct efs_sb_info *ctx, struct efs_file *f) {
  const uint32_t inodes_per_block =
      ctx->bm_inode.blk_size / sizeof(struct efs_inode);
  const uint32_t abs_blk =
      ctx->bm_inode.data_offset + f->inode_id / inodes_per_block;

  if (f->last_abs_blk != -1) efs_cache_release(&ctx->cache, f->last_abs_blk);
  efs_cache_release(&ctx->cache, abs_blk);

  memset(f, 0, sizeof(*f));
  return 0;
}

int efs_file_read(struct efs_sb_info *ctx, struct efs_file *f, int length,
                  void *buffer) {
  char *buf = (char *)buffer;
  uint32_t read_eof = f->offset + length;
  uint32_t buf_offset = 0;
  uint32_t blk_size = 1 << ctx->sb->s_log_block_size;

  if (read_eof > f->inode->i_size) read_eof = f->inode->i_size;
  while (f->offset < read_eof) {
    uint32_t curr_blk = f->offset >> ctx->sb->s_log_block_size;
    uint32_t blk_read = blk_size - (f->offset & (blk_size - 1));
    if (f->offset + blk_read > read_eof) blk_read = read_eof - f->offset;

    // fetch block
    if (curr_blk != f->last_block) {
      uint32_t curr_abs_blk;
      if (f->last_abs_blk != -1) {
        efs_cache_release(&ctx->cache, f->last_abs_blk);
        f->last_abs_blk = -1;
      }
      curr_abs_blk =
          efs_calc_abs_data_blk(ctx, f->inode, f->inode_id, curr_blk);
      if (efs_cache_fetch(&ctx->cache, curr_abs_blk, &f->data) != 0) {
        // read error
        return buf_offset;
      }
      f->last_block = curr_blk;
      f->last_abs_blk = curr_abs_blk;
    }
    // copy block to destination memory
    memcpy(buf + buf_offset, f->data + (f->offset & (blk_size - 1)), blk_read);
    buf_offset += blk_read;
    f->offset += blk_read;
  }
  return buf_offset;
}

int efs_file_write(struct efs_sb_info *ctx, struct efs_file *f, int length,
                   const void *buffer) {
  char *buf = (char *)buffer;
  uint32_t blk_size = 1 << ctx->sb->s_log_block_size;
  int buf_offset = 0;
  uint32_t required_blks =
      (f->offset + length + (1 << ctx->sb->s_log_block_size) - 1) >>
      ctx->sb->s_log_block_size;
  if (required_blks > f->inode->i_blocks) {
    if (efs_inode_expand(&ctx->inodes, f->inode_id, required_blks) != 0)
      return 0;
    f->inode->i_blocks = required_blks;
  }
  if (f->offset + length > f->inode->i_size) {
    f->inode->i_size = f->offset + length;
  }

  while (buf_offset < length) {
    uint32_t curr_blk = f->offset >> ctx->sb->s_log_block_size;
    uint32_t blk_write = blk_size - (f->offset & (blk_size - 1));
    if (buf_offset + blk_write > length) blk_write = length - buf_offset;
    if (curr_blk != f->last_block) {
      uint32_t curr_abs_blk;
      if (f->last_abs_blk != -1) {
        efs_cache_release(&ctx->cache, f->last_abs_blk);
        f->last_abs_blk = -1;
      }
      curr_abs_blk =
          efs_calc_abs_data_blk(ctx, f->inode, f->inode_id, curr_blk);
      if (efs_cache_fetch(&ctx->cache, curr_abs_blk, &f->data) != 0) {
        // read error
        return buf_offset;
      }
      efs_cache_dirty(&ctx->cache, curr_abs_blk);
      f->last_block = curr_blk;
      f->last_abs_blk = curr_abs_blk;
    } else {
      efs_cache_dirty(&ctx->cache, f->last_abs_blk);
    }
    memcpy(f->data + (f->offset & (blk_size - 1)), buf + buf_offset, blk_write);
    buf_offset += blk_write;
    f->offset += blk_write;
  }
  return buf_offset;
}

uint32_t efs_file_seek(struct efs_sb_info *ctx, struct efs_file *f, int offset,
                       int seek) {
  uint32_t target_offset;
  switch (seek) {
    case SEEK_CUR:
      target_offset = f->offset + offset;
      break;
    case SEEK_END:
      target_offset = f->inode->i_size + offset;
      break;
    case SEEK_SET:
    default:
      target_offset = offset;
      break;
  }
  uint32_t required_blks =
      (target_offset + (1 << ctx->sb->s_log_block_size) - 1) >>
      ctx->sb->s_log_block_size;
  if (required_blks > f->inode->i_blocks) {
    if (efs_inode_expand(&ctx->inodes, f->inode_id, required_blks) != 0)
      return -1;
    f->inode->i_blocks = required_blks;
  }
  if (target_offset > f->inode->i_size) {
    f->inode->i_size = target_offset;
  }
  f->offset = target_offset;
  return f->offset;
}

uint32_t efs_file_trunc(struct efs_sb_info *ctx, struct efs_file *f,
                        uint32_t new_size) {
  uint32_t required_blks = (new_size + (1 << ctx->sb->s_log_block_size) - 1) >>
                           ctx->sb->s_log_block_size;
  if (required_blks > f->inode->i_blocks) {
    efs_inode_expand(&ctx->inodes, f->inode_id, required_blks);
  } else if (required_blks < f->inode->i_blocks) {
    efs_inode_shrink(&ctx->inodes, f->inode_id, required_blks);
    if (f->offset > new_size) {
      if (f->last_abs_blk != -1) {
        efs_cache_release(&ctx->cache, f->last_abs_blk);
        f->last_block = -1;
        f->last_abs_blk = -1;
      }
      f->offset = new_size;
    }
  }
  f->inode->i_size = new_size;
  return new_size;
}
