#ifndef BITMAP_H_
#define BITMAP_H_

#include "util.h"

struct efs_bitmap {
  struct efs_cache *cache;
  uint32_t blk_offset;
  uint32_t blk_size;
  uint32_t total;
  uint32_t free;
  uint32_t data_offset;
  uint32_t last_blk;  // initial to -1
  char *data;
};

int efs_bitmap_init(struct efs_bitmap *bm, struct efs_cache *cache,
                    uint32_t blk_offset, uint32_t blk_size, uint32_t total,
                    uint32_t free, uint32_t data_offset);
int efs_bitmap_next(struct efs_bitmap *bm, uint32_t pref_next);
int efs_bitmap_set(struct efs_bitmap *bm, uint32_t curr_pos);
int efs_bitmap_clr(struct efs_bitmap *bm, uint32_t curr_pos);

#endif
