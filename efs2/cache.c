#include <stdlib.h>
#include <string.h>
#include "cache.h"
#include "util.h"

#define EFS_CACHE_HEAD 0x80000000u
#define EFS_CACHE_DIRTY 0x1u

struct efs_cache_line {
  struct list_head list;
  int id;
  int flag;
  uint32_t ref_count;
  char data[0];
};

static int efs_cache_expand(struct efs_cache *cache) {
  char *mem;
  struct efs_cache_line *ptr;
  uint32_t i;

  if (cache->blks_allocated == cache->data_size / sizeof(void *))
    return -ENOMEM;
  ++cache->blks_allocated;

  mem = (char *)malloc(cache->incr_lines * cache->line_size);
  if (mem == NULL) return -ENOMEM;
  memset(mem, 0, cache->incr_lines * cache->line_size);
  ptr = (struct efs_cache_line *)mem;
  ptr->flag |= EFS_CACHE_HEAD;

  for (i = 0; i != cache->incr_lines; i++) {
    list_add((struct list_head *)(mem + i * cache->line_size),
             &cache->cache_list);
  }
  return 0;
}

static int efs_cache_get(struct efs_cache *cache, int id, void **data) {
  struct list_head *pos;
  *data = NULL;

  for (pos = cache->locked_list.next; pos != &cache->locked_list;
       pos = pos->next) {
    struct efs_cache_line *ptr = (struct efs_cache_line *)pos;
    if (ptr->id == id) {
      ++ptr->ref_count;
      *data = ptr->data;
      list_del(pos);
      list_add(pos, &cache->locked_list);
      return 1;
    }
  }
  for (pos = cache->cache_list.next; pos != &cache->cache_list;
       pos = pos->next) {
    struct efs_cache_line *ptr = (struct efs_cache_line *)pos;
    if (ptr->id == id) {
      ++ptr->ref_count;
      *data = ptr->data;
      list_del(pos);
      list_add(pos, &cache->locked_list);
      return 1;
    }
  }
  return 0;
}

static int efs_cache_alloc(struct efs_cache *cache, int id, void **data) {
  int retval;
  struct list_head *pos;
  struct efs_cache_line *ptr;

  *data = NULL;
  if (cache->cache_list.next == &cache->cache_list) {
    if ((retval = efs_cache_expand(cache)) != 0) return retval;
  }
  pos = cache->cache_list.next;
  ptr = (struct efs_cache_line *)pos;

  if (ptr->flag & EFS_CACHE_DIRTY) {
    cache->dev->write(cache->dev, ptr->id, ptr->data);
  }
  list_del(pos);
  memset(ptr, 0, cache->line_size);

  list_add(pos, &cache->locked_list);
  ptr->id = id;
  ptr->ref_count = 1;
  ptr->flag &= EFS_CACHE_HEAD;
  *data = ptr->data;
  return 0;
}

int efs_cache_init(struct efs_cache *cache, uint32_t incr_lines,
                   uint32_t data_size, struct efs_dev *dev) {
  int retval;

  cache->incr_lines = incr_lines;
  cache->data_size = data_size;
  cache->line_size = data_size + sizeof(struct efs_cache_line);
  cache->blks_allocated = 0;
  cache->dev = dev;
  INIT_LIST_HEAD(&cache->cache_list);
  INIT_LIST_HEAD(&cache->locked_list);

  if ((retval = efs_cache_expand(cache)) != 0) return retval;
  return 0;
}

int efs_cache_fetch(struct efs_cache *cache, int id, void **data) {
  int retval;
  if (efs_cache_get(cache, id, data)) return 0;
  if ((retval = efs_cache_alloc(cache, id, data)) != 0) return retval;
  if ((retval = cache->dev->read(cache->dev, id, *data)) != 0) return retval;
  return 0;
}

int efs_cache_dirty(struct efs_cache *cache, int id) {
  struct list_head *pos;
  for (pos = cache->locked_list.next; pos != &cache->locked_list;
       pos = pos->next) {
    struct efs_cache_line *ptr = (struct efs_cache_line *)pos;
    if (ptr->id == id) {
      ptr->flag |= EFS_CACHE_DIRTY;
      list_del(pos);
      list_add(pos, &cache->locked_list);
      return 1;
    }
  }
  return 0;
}

int efs_cache_release(struct efs_cache *cache, int id) {
  struct list_head *pos;
  for (pos = cache->locked_list.next; pos != &cache->locked_list;
       pos = pos->next) {
    struct efs_cache_line *ptr = (struct efs_cache_line *)pos;
    if (ptr->id == id) {
      if (--ptr->ref_count == 0) {
        list_del(pos);
        list_add_tail(pos, &cache->cache_list);
      }
      return 1;
    }
  }
  return 0;
}

void efs_cache_flush_all(struct efs_cache *cache) {
  struct list_head *pos;
  for (pos = cache->locked_list.next; pos != &cache->locked_list;
       pos = pos->next) {
    struct efs_cache_line *ptr = (struct efs_cache_line *)pos;
    if (ptr->flag & EFS_CACHE_DIRTY) {
      cache->dev->write(cache->dev, ptr->id, ptr->data);
      ptr->flag = ptr->flag & ~EFS_CACHE_DIRTY;
    }
  }
  for (pos = cache->cache_list.next; pos != &cache->cache_list;
       pos = pos->next) {
    struct efs_cache_line *ptr = (struct efs_cache_line *)pos;
    if (ptr->flag & EFS_CACHE_DIRTY) {
      cache->dev->write(cache->dev, ptr->id, ptr->data);
      ptr->flag = ptr->flag & ~EFS_CACHE_DIRTY;
    }
  }
}

void efs_cache_free(struct efs_cache *cache) {
  struct list_head *pos;
  struct efs_cache_line *ptr;
  struct efs_cache_line **heads;
  uint32_t count, i;

  count = 0;
  heads = NULL;
  efs_cache_flush_all(cache);
  for (pos = cache->cache_list.next; pos != &cache->cache_list;
       pos = pos->next) {
    struct efs_cache_line *ptr = (struct efs_cache_line *)pos;
    if (ptr->flag & EFS_CACHE_HEAD) {
      if (heads == NULL) heads = (struct efs_cache_line **)ptr->data;
      heads[count++] = ptr;
    }
  }
  for (i = 0; i != count; i++) {
    if ((struct efs_cache_line *)heads[i]->data != heads[i]) {
      free(heads[i]);
    } else {
      ptr = heads[i];
    }
  }
  free(ptr);
}
