#ifndef FS_INT_H_
#define FS_INT_H_

#include <stdint.h>
typedef uint32_t __le32;
typedef uint16_t __le16;
typedef uint8_t __u8;

#endif
