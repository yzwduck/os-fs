#include "dev.h"
#include "util.h"

#ifdef _WIN32
static int efs_dev_read_win(struct efs_dev *dev, int id, void *data) {
  LARGE_INTEGER offset;
  LARGE_INTEGER new_offset;
  DWORD dwByteIo;

  offset.QuadPart = 1UL * dev->block_size * id;
  if (!SetFilePointerEx(dev->fd, offset, &new_offset, FILE_BEGIN)) return -EIO;
  if (new_offset.QuadPart != offset.QuadPart) return -EIO;
  if (ReadFile(dev->fd, data, dev->block_size, &dwByteIo, NULL) &&
      dwByteIo == dev->block_size)
    return 0;
  else
    return -EIO;
}

static int efs_dev_write_win(struct efs_dev *dev, int id, void *data) {
  LARGE_INTEGER offset;
  LARGE_INTEGER new_offset;
  DWORD dwByteIo;

  offset.QuadPart = 1UL * dev->block_size * id;
  if (!SetFilePointerEx(dev->fd, offset, &new_offset, FILE_BEGIN)) return -EIO;
  if (new_offset.QuadPart != offset.QuadPart) return -EIO;
  if (WriteFile(dev->fd, data, dev->block_size, &dwByteIo, NULL) &&
      dwByteIo == dev->block_size)
    return 0;
  else
    return -EIO;
}

static int efs_dev_close_win(struct efs_dev *dev) {
  if (CloseHandle(dev->fd))
    return 0;
  else
    return -EIO;
}

int efs_dev_open(struct efs_dev *dev, const char *fn) {
  memset(dev, 0, sizeof(*dev));
  dev->fd = CreateFileA(fn, GENERIC_READ | GENERIC_WRITE,
                        FILE_SHARE_READ | FILE_SHARE_DELETE, NULL, OPEN_ALWAYS,
                        FILE_ATTRIBUTE_NORMAL, NULL);
  if (dev->fd == INVALID_HANDLE_VALUE) return -EIO;
  dev->block_size = 1024;
  dev->read = efs_dev_read_win;
  dev->write = efs_dev_write_win;
  dev->close = efs_dev_close_win;
  return 0;
}

#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
static int efs_dev_read_posix(struct efs_dev *dev, int id, void *data) {
  if (pread(dev->fd, data, dev->block_size, dev->block_size * id) ==
      dev->block_size)
    return 0;
  else
    return -EIO;
}

static int efs_dev_write_posix(struct efs_dev *dev, int id, void *data) {
  if (pwrite(dev->fd, data, dev->block_size, dev->block_size * id) ==
      dev->block_size)
    return 0;
  else
    return -EIO;
}

static int efs_dev_close_posix(struct efs_dev *dev) { return close(dev->fd); }

int efs_dev_open(struct efs_dev *dev, const char *fn) {
  memset(dev, 0, sizeof(*dev));
  dev->fd = open(fn, O_CREAT | O_RDWR, 0644);
  dev->block_size = 1024;
  dev->read = efs_dev_read_posix;
  dev->write = efs_dev_write_posix;
  dev->close = efs_dev_close_posix;
  return 0;
}

#endif
