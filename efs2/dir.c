#include <string.h>
#include <stdio.h>
#include "dir.h"
#include "file.h"

uint32_t efs_dir_locate(struct efs_sb_info *ctx, uint32_t inode_id,
                        const char *fn, struct efs_dir_entry *dir_entry) {
  struct efs_file f;
  uint32_t result = -1;
  efs_file_open(ctx, inode_id, &f);
  while (f.offset != f.inode->i_size) {
    struct efs_dir_entry entry;
    memset(&entry, 0, sizeof(entry));
    efs_file_read(ctx, &f, 8, &entry);
    efs_file_read(ctx, &f, entry.rec_len - 8, (char *)&entry + 8);
    if (strcmp(entry.name, fn) == 0) {
      result = entry.inode;
      if (dir_entry) memcpy(dir_entry, &entry, sizeof(entry));
      break;
    }
  }
  efs_file_close(ctx, &f);
  return result;
}

uint32_t efs_dir_remove(struct efs_sb_info *ctx, uint32_t inode_id,
                        const char *fn, struct efs_dir_entry *dir_entry) {
  struct efs_file f1, f2;
  uint32_t result = -1;
  efs_file_open(ctx, inode_id, &f1);
  efs_file_open(ctx, inode_id, &f2);
  while (f1.offset != f1.inode->i_size) {
    struct efs_dir_entry entry;
    memset(&entry, 0, sizeof(entry));
    efs_file_read(ctx, &f1, 8, &entry);
    efs_file_read(ctx, &f1, entry.rec_len - 8, (char *)&entry + 8);
    if (strcmp(entry.name, fn) != 0) {
      efs_file_write(ctx, &f2, entry.rec_len, &entry);
    } else {
      result = 1;
      if (dir_entry) memcpy(dir_entry, &entry, sizeof(entry));
    }
  }
  efs_file_close(ctx, &f1);
  efs_file_trunc(ctx, &f2, f2.offset);
  efs_file_close(ctx, &f2);
  return result;
}

uint32_t efs_dir_remove_id(struct efs_sb_info *ctx, uint32_t inode_id,
                           uint32_t fn_inode_id,
                           struct efs_dir_entry *dir_entry) {
  struct efs_file f1, f2;
  uint32_t result = -1;
  efs_file_open(ctx, inode_id, &f1);
  efs_file_open(ctx, inode_id, &f2);
  while (f1.offset != f1.inode->i_size) {
    struct efs_dir_entry entry;
    memset(&entry, 0, sizeof(entry));
    efs_file_read(ctx, &f1, 8, &entry);
    efs_file_read(ctx, &f1, entry.rec_len - 8, (char *)&entry + 8);
    if (fn_inode_id == entry.inode) {
      efs_file_write(ctx, &f2, entry.rec_len, &entry);
    } else {
      result = 1;
      if (dir_entry) memcpy(dir_entry, &entry, sizeof(entry));
    }
  }
  efs_file_close(ctx, &f1);
  efs_file_trunc(ctx, &f2, f2.offset);
  efs_file_close(ctx, &f2);
  return result;
}

uint32_t efs_dir_append(struct efs_sb_info *ctx, uint32_t dir_inode_id,
                        uint32_t fn_inode_id, const char *fn, __u8 file_type) {
  uint32_t len;
  struct efs_dir_entry entry;
  struct efs_file f;
  memset(&entry, 0, sizeof(entry));
  len = strlen(fn);
  memcpy(entry.name, fn, len);
  entry.file_type = file_type;
  entry.name_len = len;
  entry.rec_len = ((len + 3) & (~3)) + 8;
  entry.inode = fn_inode_id;

  efs_file_open(ctx, dir_inode_id, &f);
  efs_file_seek(ctx, &f, 0, SEEK_END);
  efs_file_write(ctx, &f, entry.rec_len, &entry);
  efs_file_close(ctx, &f);
  return 0;
}

uint32_t efs_dir_resolve(struct efs_sb_info *ctx, uint32_t rel_inode_id,
                         uint32_t root_inode_id, char *path, char **fn) {
  uint32_t dir_inode;
  char *curr_fn, *next_fn;

  if (path[0] == '/')
    dir_inode = root_inode_id;
  else
    dir_inode = rel_inode_id;
  next_fn = curr_fn = path;
  while (*next_fn) {
    struct efs_dir_entry entry;
    while (*next_fn == '/') ++next_fn;
    curr_fn = next_fn;
    while (*next_fn != '/' && *next_fn != '\0') ++next_fn;
    if (*next_fn == '/') {
      *next_fn = '\0';
      ++next_fn;
    }
    if (*next_fn == '\0') break;
    dir_inode = efs_dir_locate(ctx, dir_inode, curr_fn, &entry);
    if (dir_inode == -1) return -1;
    if (entry.file_type != EFS_FT_DIR) return -1;
  }
  *fn = curr_fn;
  return dir_inode;
}
