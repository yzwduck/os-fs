#include "fs.h"
#include "dev.h"
#include "inode.h"
#include "util.h"
#include "file.h"
#include "dir.h"
#include <string.h>

#define EFS_MAGIC (('.') | ('E' << 8) | ('F' << 16) | ('S' << 24))

int efs_make(struct efs_dev *dev, size_t blk_size, size_t inodes,
             uint64_t img_size) {
  char buf[4096];
  struct efs_super_block *sb;
  struct efs_inode *inode;
  uint32_t i;
  uint32_t blocks;
  uint32_t bm_inode;
  uint32_t bm_block;
  uint32_t blks_inode;
  uint32_t trunked_size;
  uint32_t log_block_size;

  struct efs_dir_entry *dot;
  struct efs_dir_entry *dotdot;

  trunked_size = blk_size & (~blk_size + 1);
  if (trunked_size != blk_size) {
    return -1;
  }
  log_block_size = 0;
  trunked_size >>= 1;
  while (trunked_size) {
    ++log_block_size;
    trunked_size >>= 1;
  }

  // calc in bits:
  // img_size - blk_size * (1 + bm_inode + blks_inode)
  bm_inode = CEIL_DIV(inodes, 8 * blk_size);
  blks_inode = CEIL_DIV(sizeof(struct efs_inode) * inodes, blk_size);
  blocks = (img_size - blk_size * (1 + bm_inode + blks_inode)) * 8 /
           (1 + 8 * blk_size);
  bm_block = CEIL_DIV(blocks, 8 * blk_size);

  memset(buf, 0, sizeof(buf));
  sb = (struct efs_super_block *)buf;
  sb->s_magic = EFS_MAGIC;
  sb->s_inodes_count = inodes;
  sb->s_blocks_count = blocks;
  sb->s_free_inodes_count = inodes;
  sb->s_free_blocks_count = blocks;
  sb->s_first_data_block = 1 + bm_inode + bm_block + blks_inode;
  sb->s_log_block_size = log_block_size;
  dev->block_size = blk_size;
  dev->write(dev, 0, sb);

  // fill bitmap
  memset(buf, -1, sizeof(buf));
  for (i = 0; i != bm_inode + bm_block; i++) {
    dev->write(dev, 1 + i, buf);
  }

  // Create inode & data block for root directory

  // 1. clear bitmap for inode & block
  buf[0] = ~(1 << 0);
  dev->write(dev, 1, buf);
  dev->write(dev, 1 + bm_inode, buf);

  // 2. create data block for "/"
  memset(buf, 0, sizeof(buf));
  dot = (struct efs_dir_entry *)buf;
  dotdot = (struct efs_dir_entry *)(buf + 12);
  dot->inode = 0;
  dot->rec_len = 12;
  dot->name_len = 1;
  dot->file_type = EFS_FT_DIR;
  dot->name[0] = '.';
  dotdot->inode = 0;
  dotdot->rec_len = 12;
  dotdot->name_len = 2;
  dotdot->file_type = EFS_FT_DIR;
  dotdot->name[0] = '.';
  dotdot->name[1] = '.';
  dev->write(dev, 1 + bm_inode + bm_block + blks_inode, buf);

  // 3. create inode block for "/"
  memset(buf, 0, sizeof(buf));
  inode = (struct efs_inode *)buf;
  inode->i_blocks = 1;
  inode->i_size = 24;
  inode->i_mode = EFS_FT_DIR;
  inode->i_links_count = 1;
  dev->write(dev, 1 + bm_inode + bm_block, buf);

  // 4. seek to end of file
  memset(buf, 0, sizeof(buf));
  dev->write(dev, bm_inode + bm_block + blks_inode + blocks, buf);
  return 0;
}

int efs_init(struct efs_sb_info *ctx, const char *fn) {
  int retval;
  char buf[1024];
  struct efs_super_block *sb_ptr;
  uint32_t size_bm_inode, size_bm_block, size_inodes;
  uint32_t block_size;

  if ((retval = efs_dev_open(&ctx->dev, fn)) != 0) return retval;
  if ((retval = ctx->dev.read(&ctx->dev, 0, buf)) != 0) return retval;
  sb_ptr = (struct efs_super_block *)buf;
  block_size = 1 << sb_ptr->s_log_block_size;
  ctx->dev.block_size = block_size;

  efs_cache_init(&ctx->cache, 32, block_size, &ctx->dev);
  efs_cache_fetch(&ctx->cache, 0, &ctx->sb);
  ctx->dev.read(&ctx->dev, 0, ctx->sb);

  size_bm_inode = CEIL_DIV(ctx->sb->s_inodes_count, 8 * block_size);
  size_bm_block = CEIL_DIV(ctx->sb->s_blocks_count, 8 * block_size);
  size_inodes =
      CEIL_DIV(ctx->sb->s_inodes_count * sizeof(struct efs_inode), block_size);
  efs_bitmap_init(&ctx->bm_inode, &ctx->cache, 1, block_size,
                  ctx->sb->s_inodes_count, ctx->sb->s_free_inodes_count,
                  1 + size_bm_inode + size_bm_block);
  efs_bitmap_init(&ctx->bm_block, &ctx->cache, 1 + size_bm_inode, block_size,
                  ctx->sb->s_blocks_count, ctx->sb->s_free_blocks_count,
                  1 + size_bm_inode + size_bm_block + size_inodes);
  ctx->inodes.bm_block = &ctx->bm_block;
  ctx->inodes.bm_inode = &ctx->bm_inode;
  ctx->inodes.log_index_per_blk = sb_ptr->s_log_block_size - 2;
  return 0;
}

char buffer[8192];
char buf2[8192];

int main123() {
  int i;
  struct efs_sb_info ctx;
  struct efs_dev dev;
  int z = 16384;
  struct efs_file f;
  struct efs_dir_entry entry;
  char *fn;
  char orig_fn[] = "beta.tx";
  efs_dev_open(&dev, "efs.img");
  efs_make(&dev, 1024, z, 1024 * 1024 * 64);
  dev.close(&dev);
  /*
          efs_init(&ctx, "efs.img");
          efs_dir_append(&ctx, 0, 0xbeef, "new_file.txt", EFS_FT_REG_FILE);
          efs_dir_append(&ctx, 0, 0x3412, "yet another file.bn",
    EFS_FT_REG_FILE);
    efs_dir_resolve(&ctx, 0, 0, orig_fn, &fn);
          efs_cache_flush_all(&ctx.cache);
          efs_dir_remove(&ctx, 0, "new_file.txt", &entry);

          for (i = 0; i < 8192; i++) buffer[i] = i % 255;
          efs_file_open(&ctx, 0, &f);
          for (i = 0; i < 1024 * 1024 * 16; i++) {
                  efs_file_write(&ctx, &f, 1, buffer);
                  // efs_cache_flush_all(&ctx.cache);
          }
          efs_file_seek(&ctx, &f, 0, 0);
          for (int i = 0; i < 1024; i++) {
                  memset(buf2, 0, sizeof(buf2));
                  efs_file_read(&ctx, &f, 8191, buf2);
                  if (memcmp(buffer, buf2, 8191) != 0) {
                          int dead = 1;
                  }
          }
          efs_file_close(&ctx, &f);
          */
  // efs_inode_expand(&ctx.inodes, 0, 14);
  // efs_inode_shrink(&ctx.inodes, 0, 1);
  // efs_cache_flush_all(&ctx.cache);

  // ctx.dev.close(&ctx.dev);
  return 0;
}
