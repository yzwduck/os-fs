#ifndef CACHE_H_
#define CACHE_H_

#include "util.h"
#include "list.h"
#include "dev.h"

struct efs_cache {
  struct efs_dev *dev;
  struct list_head cache_list;
  struct list_head locked_list;
  int incr_lines;
  int data_size;
  int line_size;
  int blks_allocated;
};

int efs_cache_init(struct efs_cache *cache, uint32_t incr_lines,
                   uint32_t data_size, struct efs_dev *dev);
int efs_cache_fetch(struct efs_cache *cache, int id, void **data);
int efs_cache_dirty(struct efs_cache *cache, int id);
int efs_cache_release(struct efs_cache *cache, int id);
void efs_cache_flush_all(struct efs_cache *cache);
void efs_cache_free(struct efs_cache *cache);

#endif
