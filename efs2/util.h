#ifndef UTIL_H_
#define UTIL_H_

#include <stdint.h>
#include "errno.h"

#define CEIL_DIV(a, b) (((a) + (b)-1) / (b))

#endif
