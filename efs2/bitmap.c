#include <stdlib.h>
#include "bitmap.h"
#include "cache.h"

static int efs_bitmap_lock(struct efs_bitmap *bm, uint32_t curr_blk) {
  uint32_t abs_blk = curr_blk + bm->blk_offset;
  if (curr_blk != bm->last_blk) {
    int retval;
    if (bm->last_blk != -1)
      efs_cache_release(bm->cache, bm->last_blk + bm->blk_offset);
    bm->last_blk = curr_blk;
    if ((retval = efs_cache_fetch(bm->cache, abs_blk, &bm->data)) != 0)
      return retval;
  }
  return 0;
}

static int efs_bitmap_release(struct efs_bitmap *bm) {
  uint32_t abs_blk = bm->last_blk + bm->blk_offset;
  // return efs_cache_release(bm->cache, abs_blk);
  return 0;
}

static int efs_bitmap_dirty(struct efs_bitmap *bm) {
  uint32_t abs_blk = bm->last_blk + bm->blk_offset;
  return efs_cache_dirty(bm->cache, abs_blk);
}

int efs_bitmap_init(struct efs_bitmap *bm, struct efs_cache *cache,
                    uint32_t blk_offset, uint32_t blk_size, uint32_t total,
                    uint32_t free, uint32_t data_offset) {
  bm->cache = cache;
  bm->blk_offset = blk_offset;
  bm->blk_size = blk_size;
  bm->total = total;
  bm->free = free;
  bm->data_offset = data_offset;
  bm->last_blk = -1;
  bm->data = NULL;
  return 0;
}

int efs_bitmap_next(struct efs_bitmap *bm, uint32_t pref_next) {
  uint32_t curr_pos;
  if (bm->free == 0) return -1;

  for (curr_pos = pref_next; curr_pos < bm->total; curr_pos++) {
    uint32_t curr_blk = curr_pos / (bm->blk_size * 8);
    uint32_t offset_byte = curr_pos / 8 - curr_blk * bm->blk_size;
    uint32_t offset_bit = curr_pos % 8;

    if (efs_bitmap_lock(bm, curr_blk)) return -1;
    if (bm->data[offset_byte] & (1 << offset_bit)) {
      // free found
      efs_bitmap_release(bm);
      return curr_pos;
    } else {
      // occupied
      continue;
    }
  }
  for (curr_pos = 0; curr_pos < pref_next; curr_pos++) {
    uint32_t curr_blk = curr_pos / (bm->blk_size * 8);
    uint32_t offset_byte = curr_pos / 8 - curr_blk * bm->blk_size;
    uint32_t offset_bit = curr_pos % 8;

    if (efs_bitmap_lock(bm, curr_blk)) return -1;
    if (bm->data[offset_byte] & (1 << offset_bit)) {
      // free found
      efs_bitmap_release(bm);
      return curr_pos;
    } else {
      // occupied
      continue;
    }
  }
  return -1;
}

int efs_bitmap_set(struct efs_bitmap *bm, uint32_t curr_pos) {
  uint32_t curr_blk = curr_pos / (bm->blk_size * 8);
  uint32_t offset_byte = curr_pos / 8 - curr_blk * bm->blk_size;
  uint32_t offset_bit = curr_pos % 8;
  uint32_t abs_blk = curr_blk + bm->blk_offset;

  if (efs_bitmap_lock(bm, curr_blk) != 0) return -1;
  if (bm->data[offset_byte] & (1 << offset_bit)) {
    --bm->free;
    bm->data[offset_byte] &= ~(1 << offset_bit);
    if (efs_bitmap_dirty(bm) != 0) return -1;
  }
  efs_bitmap_release(bm);
  return 0;
}

int efs_bitmap_clr(struct efs_bitmap *bm, uint32_t curr_pos) {
  uint32_t curr_blk = curr_pos / (bm->blk_size * 8);
  uint32_t offset_byte = curr_pos / 8 - curr_blk * bm->blk_size;
  uint32_t offset_bit = curr_pos % 8;
  uint32_t abs_blk = curr_blk + bm->blk_offset;

  if (efs_bitmap_lock(bm, curr_blk) != 0) return -1;
  if (!(bm->data[offset_byte] & (1 << offset_bit))) {
    ++bm->free;
    bm->data[offset_byte] |= 1 << offset_bit;
    if (efs_bitmap_dirty(bm) != 0) return -1;
  }
  efs_bitmap_release(bm);
  return 0;
}
