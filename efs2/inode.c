#include "inode.h"
#include "cache.h"

/**
 * 计算出inode中第i_block个数据块编号，位于索引树的哪个分支上。
 */
int efs_block_to_path(struct efs_inode_info *ctx, uint32_t i_block,
                      uint32_t offsets[4]) {
  uint32_t ptrs = 1u << ctx->log_index_per_blk;
  uint32_t ptrs_bits = ctx->log_index_per_blk;
  const uint32_t direct_blocks = EFS_NDIR_BLOCKS, indirect_blocks = ptrs,
                 double_blocks = 1u << (ptrs_bits * 2);
  int n = 0;
  offsets[0] = offsets[1] = offsets[2] = offsets[3] = -1;

  if (i_block < direct_blocks) {
    offsets[n++] = i_block;
  } else if ((i_block -= direct_blocks) < indirect_blocks) {
    offsets[n++] = EFS_IND_BLOCK;
    offsets[n++] = i_block;
  } else if ((i_block -= indirect_blocks) < double_blocks) {
    offsets[n++] = EFS_DIND_BLOCK;
    offsets[n++] = i_block >> ptrs_bits;
    offsets[n++] = i_block & (ptrs - 1);
  } else if ((i_block -= double_blocks) >> (ptrs_bits * 2) < ptrs) {
    offsets[n++] = EFS_TIND_BLOCK;
    offsets[n++] = i_block >> (ptrs_bits * 2);
    offsets[n++] = (i_block >> ptrs_bits) & (ptrs - 1);
    offsets[n++] = i_block & (ptrs - 1);
  } else {
    n = -1;
  }
  return n;
}

/**
 * 判断指定节点是否为某一块索引表的第一个
 */
void efs_path_is_first_in_block(uint32_t offsets[4], char is_first[4]) {
  int indirect_lv;
  int i;
  is_first[0] = is_first[1] = is_first[2] = is_first[3] = 0;
  if (offsets[0] < EFS_NDIR_BLOCKS) return;
  indirect_lv = offsets[0] - EFS_NDIR_BLOCKS + 1;
  for (i = indirect_lv; i > 0 && offsets[i] == 0; i--) {
    is_first[i] = 1;
  }
}

int efs_inode_expand(struct efs_inode_info *ctx, uint32_t inode_id,
                     uint32_t new_blocks) {
  int retval;
  int i;

  struct efs_inode *inode_block, *inode;
  uint32_t *blk[4];
  uint32_t abs_blk[4] = {-1, -1, -1, -1};
  uint32_t inode_index;
  uint32_t new_block = 0;

  // 计算 inode 位置
  const uint32_t inodes_per_block =
      ctx->bm_inode->blk_size / sizeof(struct efs_inode);
  abs_blk[0] = ctx->bm_inode->data_offset + inode_id / inodes_per_block;
  inode_index = inode_id % inodes_per_block;
  // 获取 inode
  if ((retval = efs_cache_fetch(ctx->bm_inode->cache, abs_blk[0],
                                &inode_block)) != 0) {
    return retval;
  }
  efs_cache_dirty(ctx->bm_inode->cache, abs_blk[0]);
  inode = &inode_block[inode_index];
  blk[0] = inode->i_block;

  // 逐一申请块
  for (; inode->i_blocks != new_blocks; inode->i_blocks++) {
    int n;
    uint32_t offsets[4];
    char is_first[4];
    // 计算新块位置
    n = efs_block_to_path(ctx, inode->i_blocks, offsets);
    efs_path_is_first_in_block(offsets, is_first);
    // 保证块表合法
    for (i = 1; i < n; i++) {
      uint32_t new_abs_blk;
      if (is_first[i]) {
        // 新一层块表，申请空间
        new_block = efs_bitmap_next(ctx->bm_block, new_block);
        if (new_block == -1) {
          // error
          int dead = 1;
        }
        efs_bitmap_set(ctx->bm_block, new_block);
        // 更新上层块表的指针
        blk[i - 1][offsets[i - 1]] = new_block;
      }
      // 读取当前的块表
      new_abs_blk = blk[i - 1][offsets[i - 1]] + ctx->bm_block->data_offset;
      if (new_abs_blk != abs_blk[i]) {
        if (abs_blk[i] != -1) {
          efs_cache_release(ctx->bm_block->cache, abs_blk[i]);
          abs_blk[i] = -1;
        }
        efs_cache_fetch(ctx->bm_block->cache, new_abs_blk, &blk[i]);
        efs_cache_dirty(ctx->bm_block->cache, new_abs_blk);
        abs_blk[i] = new_abs_blk;
      }
    }
    // 申请新数据节点
    new_block = efs_bitmap_next(ctx->bm_block, new_block);
    if (new_block == -1) {
      // error
      int dead = 1;
    }
    efs_bitmap_set(ctx->bm_block, new_block);
    // 保存到块表中
    blk[n - 1][offsets[n - 1]] = new_block;
  }

  for (i = 0; i < 4; i++)
    if (abs_blk[i] != -1) efs_cache_release(ctx->bm_block->cache, abs_blk[i]);
  // efs_cache_flush_all(ctx->bm_block->cache);
  return 0;
}

int efs_inode_shrink(struct efs_inode_info *ctx, uint32_t inode_id,
                     uint32_t new_blocks) {
  int retval;
  int i;

  struct efs_inode *inode_block, *inode;
  uint32_t *blk[4];
  uint32_t abs_blk[4];
  uint32_t inode_index;
  uint32_t last_block = 0;

  const uint32_t inodes_per_block =
      ctx->bm_inode->blk_size / sizeof(struct efs_inode);
  abs_blk[0] = ctx->bm_inode->data_offset + inode_id / inodes_per_block;
  inode_index = inode_id % inodes_per_block;

  if ((retval = efs_cache_fetch(ctx->bm_inode->cache, abs_blk[0],
                                &inode_block)) != 0) {
    return retval;
  }
  efs_cache_dirty(ctx->bm_inode->cache, abs_blk[0]);
  inode = &inode_block[inode_index];
  blk[0] = inode->i_block;

  for (; inode->i_blocks != new_blocks; inode->i_blocks--) {
    int n;
    uint32_t offsets[4];
    char is_first[4];
    n = efs_block_to_path(ctx, inode->i_blocks - 1, offsets);
    efs_path_is_first_in_block(offsets, is_first);
    for (i = 1; i < n; i++) {
      uint32_t new_abs_blk;
      new_abs_blk = blk[i - 1][offsets[i - 1]] + ctx->bm_block->data_offset;
      if (new_abs_blk != abs_blk[i]) {
        if (abs_blk[i] != -1) {
          efs_cache_release(ctx->bm_block->cache, abs_blk[i]);
          abs_blk[i] = -1;
        }
        efs_cache_fetch(ctx->bm_block->cache, new_abs_blk, &blk[i]);
        efs_cache_dirty(ctx->bm_block->cache, new_abs_blk);
        abs_blk[i] = new_abs_blk;
      }
    }
    // 清空当前数据块的标记
    last_block = blk[n - 1][offsets[n - 1]];
    efs_bitmap_clr(ctx->bm_block, last_block);
    blk[n - 1][offsets[n - 1]] = 0;

    for (i = n - 1; i > 0; i--) {
      // 如果当前数据块所属索引表已空
      if (is_first[i]) {
        efs_cache_release(ctx->bm_block->cache, abs_blk[i]);
        abs_blk[i] = -1;
        efs_bitmap_clr(ctx->bm_block, blk[i - 1][offsets[i - 1]]);
        blk[i - 1][offsets[i - 1]] = 0;
      }
    }
  }
  return 0;
}
