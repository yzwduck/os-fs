#ifndef INODE_H_
#define INODE_H_

#include "bitmap.h"

#define EFS_NDIR_BLOCKS 12
#define EFS_IND_BLOCK EFS_NDIR_BLOCKS
#define EFS_DIND_BLOCK (EFS_IND_BLOCK + 1)
#define EFS_TIND_BLOCK (EFS_DIND_BLOCK + 1)
#define EFS_N_BLOCKS (EFS_TIND_BLOCK + 1)

struct efs_inode {
  // 0x00
  uint16_t i_mode;
  uint16_t i_uid;
  uint32_t i_size;
  uint32_t i_atime;
  uint32_t i_ctime;
  // 0x10
  uint32_t i_mtime;
  uint32_t i_dtime;
  uint16_t i_gid;
  uint16_t i_links_count;
  uint32_t i_blocks;
  // 0x20
  uint32_t i_flags;

  uint32_t i_block[EFS_N_BLOCKS];
  uint32_t i_reversed[8];
};

struct efs_inode_info {
  struct efs_bitmap *bm_inode;
  struct efs_bitmap *bm_block;
  uint32_t last_inode_id;
  uint32_t log_index_per_blk;
};

int efs_block_to_path(struct efs_inode_info *ctx, uint32_t i_block,
                      uint32_t offsets[4]);
void efs_path_is_first_in_block(uint32_t offsets[4], char is_first[4]);
int efs_inode_expand(struct efs_inode_info *ctx, uint32_t inode_id,
                     uint32_t new_blocks);
int efs_inode_shrink(struct efs_inode_info *ctx, uint32_t inode_id,
                     uint32_t new_blocks);
#endif
