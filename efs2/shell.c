#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fs.h"
#include "inode.h"
#include "dir.h"
#include "file.h"

#define RL_BUFSIZE 256
#define PRINT_ERR_ALLOC()                      \
  do {                                         \
    fprintf(stderr, "sh: allocation error\n"); \
    exit(EXIT_FAILURE);                        \
  } while (0)

static struct efs_sb_info ctx;
static uint32_t cwd_inode;
static struct efs_file *fps;
int n_fps;

static char *sh_read_line() {
  int bufsize = RL_BUFSIZE;
  int position = 0;
  char *buffer = (char *)malloc(sizeof(char) * bufsize);
  int c;

  if (!buffer) {
    PRINT_ERR_ALLOC();
  }
  while (1) {
    c = getchar();
    if (c == EOF || c == '\n') {
      buffer[position] = '\0';
      return buffer;
    } else {
      buffer[position] = c;
    }
    position++;

    if (position >= bufsize) {
      bufsize += RL_BUFSIZE;
      buffer = (char *)realloc(buffer, bufsize);
      if (!buffer) {
        PRINT_ERR_ALLOC();
      }
    }
  }
}

static int is_space(char ch) { return ch == ' ' || ch == '\t'; }
static int is_hex(char ch) {
  if ('0' <= ch && ch <= '9') return ch - '0';
  if ('a' <= ch && ch <= 'f') return ch - 'a' + 10;
  if ('A' <= ch && ch <= 'F') return ch - 'A' + 10;
  return -1;
}

static char *str_next_param(char *cmd) {
  char quote = '\0';
  char *ptr_write = cmd;
  while (is_space(*cmd)) ++cmd;
  if (*cmd == '"' || *cmd == '\'') {
    quote = *cmd;
    ++cmd;
  }
  while (*cmd && (quote ? *cmd != quote : !is_space(*cmd))) {
    if (*cmd == '\\') {
      ++cmd;
      if (*cmd == '\0') break;
      if (quote != '\0') {
        switch (*cmd) {
          case 'n':
            *cmd = '\n';
            break;
          case 't':
            *cmd = '\t';
            break;
          case 'x':
          case 'X':
            if (is_hex(cmd[1]) != -1 && is_hex(cmd[2]) != -1) {
              cmd[2] = is_hex(cmd[1]) * 16 + is_hex(cmd[2]);
              cmd += 2;
            } else {
              ++cmd;
            }
            break;
        }
      }
    }
    *ptr_write++ = *cmd++;
  }
  if (*cmd != '\0') ++cmd;
  while (is_space(*cmd)) ++cmd;
  *ptr_write = '\0';
  return cmd;
}

static int next_free_fp() {
  int i;
  for (i = 2; i < n_fps; i++) {
    if (fps[i].inode_id == -1) return i;
  }
  fps = (struct efs_file *)realloc(fps, sizeof(fps[0]) * n_fps * 2);
  for (i = n_fps; i < n_fps * 2; i++) fps[i].inode_id = -1;
  n_fps = n_fps * 2;
  return n_fps / 2;
}

static int execute_cd(char *param) {
  uint32_t new_dir_inode;
  char *fn;
  new_dir_inode = efs_dir_resolve(&ctx, cwd_inode, 0, param, &fn);
  if (new_dir_inode != -1) {
    struct efs_dir_entry entry;
    if (efs_dir_locate(&ctx, new_dir_inode, fn, &entry) != -1) {
      if (entry.file_type == EFS_FT_DIR) {
        cwd_inode = entry.inode;
        return 1;
      }
    }
  }
  return 0;
}

static int execute_mkdir(char *param) {
  uint32_t parent_dir_inode;
  char *fn;
  parent_dir_inode = efs_dir_resolve(&ctx, cwd_inode, 0, param, &fn);
  if (parent_dir_inode != -1) {
    struct efs_dir_entry entry;
    if (efs_dir_locate(&ctx, parent_dir_inode, fn, &entry) == -1) {
      // not exist
      struct efs_file f;
      struct efs_inode inode;
      memset(&inode, 0, sizeof(inode));
      inode.i_links_count = 1;
      if (efs_file_creat(&ctx, &inode, &f) == 0) {
        char buf[24];
        memset(buf, 0, sizeof(buf));
        uint32_t new_dir_inode = f.inode_id;
        struct efs_dir_entry *dot, *dotdot;
        dot = (struct efs_dir_entry *)buf;
        dotdot = (struct efs_dir_entry *)(buf + 12);
        dot->inode = f.inode_id;
        dot->rec_len = 12;
        dot->name_len = 1;
        dot->file_type = EFS_FT_DIR;
        dot->name[0] = '.';
        dotdot->inode = parent_dir_inode;
        dotdot->rec_len = 12;
        dotdot->name_len = 2;
        dotdot->file_type = EFS_FT_DIR;
        dotdot->name[0] = '.';
        dotdot->name[1] = '.';
        efs_file_write(&ctx, &f, 24, buf);
        efs_file_close(&ctx, &f);
        if (efs_dir_append(&ctx, parent_dir_inode, new_dir_inode, fn,
                           EFS_FT_DIR) == 0) {
          return 1;
        }
      }
    }
  }
  return 0;
}

static int execute_creat(char *param) {
  uint32_t parent_dir_inode;
  struct efs_inode inode;
  char *fn;
  int fp;
  struct efs_file *f;
  fp = next_free_fp();
  f = &fps[fp];
  parent_dir_inode = efs_dir_resolve(&ctx, cwd_inode, 0, param, &fn);
  if (parent_dir_inode == -1) {
    return 0;
  }
  if (efs_dir_locate(&ctx, parent_dir_inode, fn, NULL) != -1) {
    return 0;
  }
  memset(&inode, 0, sizeof(inode));
  inode.i_links_count = 1;

  if (efs_file_creat(&ctx, &inode, f) != 0) return 0;
  efs_dir_append(&ctx, parent_dir_inode, f->inode_id, fn, EFS_FT_REG_FILE);
  return fp;
}

static int execute_open(char *param) {
  uint32_t parent_dir_inode;
  struct efs_dir_entry entry;
  char *fn;
  int fp;
  struct efs_file *f;
  fp = next_free_fp();
  f = &fps[fp];
  parent_dir_inode = efs_dir_resolve(&ctx, cwd_inode, 0, param, &fn);
  if (parent_dir_inode == -1) {
    return 0;
  }
  if (efs_dir_locate(&ctx, parent_dir_inode, fn, &entry) == -1) {
    return 0;
  }
  if (entry.file_type != EFS_FT_REG_FILE) {
    return 0;
  }
  if (efs_file_open(&ctx, entry.inode, f) != 0) {
    return 0;
  }
  return fp;
}

static int execute_close(int fp) {
  if (fp <= 2) return 0;
  if (fps[fp].inode_id == -1) return 0;
  efs_file_close(&ctx, &fps[fp]);
  fps[fp].inode_id = -1;
  return 1;
}

static int execute_ls(char *path) {
  uint32_t dir_inode = cwd_inode;
  struct efs_file f;
  struct efs_dir_entry entry;
  if (*path) {
    char *fn;
    struct efs_dir_entry entry;
    dir_inode = efs_dir_resolve(&ctx, cwd_inode, 0, path, &fn);
    if (dir_inode == -1) return 0;
    if (*fn) {
      dir_inode = efs_dir_locate(&ctx, dir_inode, fn, &entry);
      if (dir_inode == -1) return 0;
      if (entry.file_type != EFS_FT_DIR) return 0;
    }
  }
  if (efs_file_open(&ctx, dir_inode, &f) != 0) return 0;
  while (f.offset != f.inode->i_size) {
    memset(&entry, 0, sizeof(entry));
    efs_file_read(&ctx, &f, 8, &entry);
    efs_file_read(&ctx, &f, entry.rec_len - 8, entry.name);
    fprintf(stdout, "%c %s\n", entry.file_type == EFS_FT_DIR ? 'd' : ' ',
            entry.name);
  }
  efs_file_close(&ctx, &f);
  return 1;
}

static int execute_tell(int fd) {
  if (fd <= 2) return 0;
  if (fps[fd].inode_id == -1) return 0;
  fprintf(stdout, "pointer at %d (0x%x)\n", fps[fd].offset, fps[fd].offset);
  return 1;
}

static void print_hex(char *buf, int length) {
  int i, j;
  for (i = 0; i < length; i += 16) {
    for (j = 0; j < 16; j++) {
      if (i + j < length)
        printf("%02X ", buf[i + j]);
      else
        printf("   ");
    }
    printf("| ");
    for (j = 0; j < 16; j++) {
      if (i + j < length) {
        char c = buf[i + j];
        if (c < 0x20) c = '.';
        if (c < 0) c = '.';
        printf("%c", c);
      } else {
        printf(" ");
      }
    }
    printf("\n");
  }
}

static int execute_read(int fp, int bytes) {
  if (fp <= 2) return 0;
  if (fps[fp].inode_id == -1) return 0;
  char *buf = (char *)malloc(bytes);
  int retval = efs_file_read(&ctx, &fps[fp], bytes, buf);
  print_hex(buf, retval);
  free(buf);
  return 1;
}

static int execute_write(int fp, const char *buf, int rep) {
  int len = strlen(buf);
  if (fp <= 2) return 0;
  if (fps[fp].inode_id == -1) return 0;
  int i;
  for (i = 0; i < rep; i++) efs_file_write(&ctx, &fps[fp], len, buf);
  return 1;
}

static int execute_seek(int fp, int pos, int seek) {
  if (fp <= 2) return 0;
  if (fps[fp].inode_id == -1) return 0;
  return efs_file_seek(&ctx, &fps[fp], pos, seek) != -1;
}

static int execute_delete(char *param) {
  uint32_t parent_dir_inode;
  struct efs_dir_entry entry;
  char *fn;

  parent_dir_inode = efs_dir_resolve(&ctx, cwd_inode, 0, param, &fn);
  if (parent_dir_inode == -1) return 0;
  if (!efs_dir_remove(&ctx, parent_dir_inode, fn, &entry)) return 0;
  efs_file_delete(&ctx, entry.inode);
  return 1;
}

static int execute_move(char *path1, char *path2) {
  uint32_t parent_dir_inode1, parent_dir_inode2;
  char *fn1, *fn2;

  parent_dir_inode1 = efs_dir_resolve(&ctx, cwd_inode, 0, path1, &fn1);
  parent_dir_inode2 = efs_dir_resolve(&ctx, cwd_inode, 0, path2, &fn2);
  if (*fn1 && *fn2 && parent_dir_inode1 != -1 && parent_dir_inode2 != -1) {
    struct efs_dir_entry entry;
    uint32_t fn_inode;
    if (!efs_dir_remove(&ctx, parent_dir_inode1, fn1, &entry)) return 0;
    efs_dir_append(&ctx, parent_dir_inode2, entry.inode, fn2, entry.file_type);
    return 1;
  }
  return 0;
}

static int execute_trunc(int fp, int size) {
  if (fp <= 2) return 0;
  if (fps[fp].inode_id == -1) return 0;
  efs_file_trunc(&ctx, &fps[fp], size);
  return 1;
}

static int execute_stat(char *param) {
  uint32_t parent_dir_inode;
  struct efs_dir_entry entry;
  char *fn;
  struct efs_file f;
  char buf[64];
  int i;
  parent_dir_inode = efs_dir_resolve(&ctx, cwd_inode, 0, param, &fn);
  if (parent_dir_inode == -1) {
    return 0;
  }
  if (efs_dir_locate(&ctx, parent_dir_inode, fn, &entry) == -1) {
    return 0;
  }
  if (entry.file_type != EFS_FT_REG_FILE) {
    return 0;
  }
  if (efs_file_open(&ctx, entry.inode, &f) != 0) {
    return 0;
  }
  fprintf(stdout, "size = %d\n", f.inode->i_size);
  fprintf(stdout, "blocks = %d\n", f.inode->i_blocks);
  efs_file_close(&ctx, &f);
  return 1;
}

static int execute_hexdump(char *param) {
  uint32_t parent_dir_inode;
  struct efs_dir_entry entry;
  char *fn;
  struct efs_file f;
  char buf[64];
  int i;
  parent_dir_inode = efs_dir_resolve(&ctx, cwd_inode, 0, param, &fn);
  if (parent_dir_inode == -1) {
    return 0;
  }
  if (efs_dir_locate(&ctx, parent_dir_inode, fn, &entry) == -1) {
    return 0;
  }
  if (entry.file_type != EFS_FT_REG_FILE) {
    return 0;
  }
  if (efs_file_open(&ctx, entry.inode, &f) != 0) {
    return 0;
  }
  do {
    i = efs_file_read(&ctx, &f, 64, buf);
    print_hex(buf, i);
  } while (i > 0);
  efs_file_close(&ctx, &f);
  return 1;
}
static int execute_cat(char *param) {
  uint32_t parent_dir_inode;
  struct efs_dir_entry entry;
  char *fn;
  struct efs_file f;
  char buf[64];
  int i;
  parent_dir_inode = efs_dir_resolve(&ctx, cwd_inode, 0, param, &fn);
  if (parent_dir_inode == -1) {
    return 0;
  }
  if (efs_dir_locate(&ctx, parent_dir_inode, fn, &entry) == -1) {
    return 0;
  }
  if (entry.file_type != EFS_FT_REG_FILE) {
    return 0;
  }
  if (efs_file_open(&ctx, entry.inode, &f) != 0) {
    return 0;
  }
  do {
    i = efs_file_read(&ctx, &f, 64, buf);
    fwrite(buf, i, 1, stdout);
  } while (i > 0);
  efs_file_close(&ctx, &f);
  fprintf(stdout, "\n");
  return 1;
}

static int execute_cmd(char *cmd) {
  char *param;
  int succeed;
  if (*cmd == '\0') return 0;
  param = str_next_param(cmd);
  if (strcmp(cmd, "exit") == 0) {
    efs_cache_flush_all(&ctx.cache);
    exit(0);
    return 0;
  } else if (strcmp(cmd, "cd") == 0) {
    str_next_param(param);
    succeed = execute_cd(param);
  } else if (strcmp(cmd, "mkdir") == 0) {
    str_next_param(param);
    succeed = execute_mkdir(param);
  } else if (strcmp(cmd, "ls") == 0) {
    str_next_param(param);
    succeed = execute_ls(param);
  } else if (strcmp(cmd, "open") == 0) {
    str_next_param(param);
    succeed = execute_open(param);
    if (succeed) fprintf(stdout, "file opened, handle: %d\n", succeed);
  } else if (strcmp(cmd, "stat") == 0) {
    str_next_param(param);
    succeed = execute_stat(param);
  } else if (strcmp(cmd, "creat") == 0) {
    str_next_param(param);
    succeed = execute_creat(param);
    if (succeed) fprintf(stdout, "new file opened, handle: %d\n", succeed);
  } else if (strcmp(cmd, "open") == 0) {
    str_next_param(param);
    succeed = execute_open(param);
    if (succeed) fprintf(stdout, "file opened, handle: %d\n", succeed);
  } else if (strcmp(cmd, "close") == 0) {
    str_next_param(param);
    int fp = atoi(param);
    succeed = execute_close(fp);
  } else if (strcmp(cmd, "read") == 0) {
    char *c_fp, *c_bytes;
    int fp, bytes;
    c_fp = param;
    c_bytes = str_next_param(c_fp);
    param = str_next_param(c_bytes);
    fp = atoi(c_fp);
    bytes = atoi(c_bytes);
    succeed = execute_read(fp, bytes);
  } else if (strcmp(cmd, "write") == 0) {
    char *c_fp, *buf, *c_rep;
    int fp, rep;
    c_fp = param;
    buf = str_next_param(c_fp);
    c_rep = str_next_param(buf);
    param = str_next_param(c_rep);
    fp = atoi(c_fp);
    rep = atoi(c_rep);
    if (rep == 0) rep = 1;
    succeed = execute_write(fp, buf, rep);
  } else if (strcmp(cmd, "seek") == 0) {
    char *c_fp, *c_pos, *c_seek;
    int fp, pos, seek;
    c_fp = param;
    c_pos = str_next_param(c_fp);
    c_seek = str_next_param(c_pos);
    param = str_next_param(c_seek);
    fp = atoi(c_fp);
    pos = atoi(c_pos);
    seek = atoi(c_seek);
    succeed = execute_seek(fp, pos, seek);
  } else if (strcmp(cmd, "tell") == 0) {
    char *c_fp;
    int fp;
    c_fp = param;
    str_next_param(c_fp);
    fp = atoi(c_fp);
    succeed = execute_tell(fp);
  } else if (strcmp(cmd, "trunc") == 0) {
    char *c_fp, *c_size;
    int fp, size;
    c_fp = param;
    c_size = str_next_param(c_fp);
    str_next_param(c_size);
    fp = atoi(c_fp);
    size = atoi(c_size);
    succeed = execute_trunc(fp, size);
  } else if (strcmp(cmd, "mv") == 0) {
    char *fn1, *fn2;
    fn1 = param;
    fn2 = str_next_param(fn1);
    str_next_param(fn2);
    succeed = execute_move(fn1, fn2);
  } else if (strcmp(cmd, "rm") == 0) {
    str_next_param(param);
    succeed = execute_delete(param);
  } else if (strcmp(cmd, "help") == 0) {
  } else if (strcmp(cmd, "cat") == 0) {
    str_next_param(param);
    succeed = execute_cat(param);
  } else if (strcmp(cmd, "hexdump") == 0) {
    str_next_param(param);
    succeed = execute_hexdump(param);
  } else {
    fprintf(stdout, "unknown command error, ");
    succeed = 0;
  }
  fprintf(stdout, "%s %s\n", cmd, succeed ? "succeed" : "failed");
  return 0;
}

int main() {
  int i;
  fprintf(stderr, "fs shell\n");
  if (efs_init(&ctx, "efs.img") != 0) {
    fprintf(stderr, "cannot load disk image\n");
    exit(-1);
  }
  fps = (struct efs_file *)malloc(sizeof(fps[0]) * 8);
  n_fps = 8;
  for (i = 3; i < n_fps; i++) fps[i].inode_id = -1;
  cwd_inode = 0;
  fprintf(stderr, "disk image loaded\n\n");
  while (1) {
    char *cmd;
    fprintf(stdout, "> ");
    fflush(stdout);
    cmd = sh_read_line();
    execute_cmd(cmd);
    free(cmd);
  }
  return 0;
}
