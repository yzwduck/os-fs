#ifndef FILE_H_
#define FILE_H_

#include "util.h"
#include "fs.h"

struct efs_file {
  uint32_t inode_id;
  uint32_t last_block;
  uint32_t last_abs_blk;
  char *data;
  struct efs_inode *inode;
  uint32_t offset;
};

int efs_file_creat(struct efs_sb_info *ctx, struct efs_inode *inode,
                   struct efs_file *f);
int efs_file_delete(struct efs_sb_info *ctx, uint32_t inode_id);
int efs_file_open(struct efs_sb_info *ctx, uint32_t inode_id,
                  struct efs_file *f);
int efs_file_close(struct efs_sb_info *ctx, struct efs_file *f);
int efs_file_read(struct efs_sb_info *ctx, struct efs_file *f, int length,
                  void *buf);
int efs_file_write(struct efs_sb_info *ctx, struct efs_file *f, int length,
                   const void *buf);
uint32_t efs_file_seek(struct efs_sb_info *ctx, struct efs_file *f, int offset,
                       int seek);
uint32_t efs_file_trunc(struct efs_sb_info *ctx, struct efs_file *f,
                        uint32_t new_size);

#endif
