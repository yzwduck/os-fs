#ifndef DEV_H_
#define DEV_H_

#ifdef _WIN32
#include <Windows.h>
#endif

struct efs_dev {
#ifdef _WIN32
  HANDLE fd;
#else
  int fd;
#endif
  int block_size;
  int (*read)(struct efs_dev *ctx, int id, void *data);
  int (*write)(struct efs_dev *ctx, int id, void *data);
  int (*close)(struct efs_dev *ctx);
};

int efs_dev_open(struct efs_dev *dev, const char *fn);

#endif
