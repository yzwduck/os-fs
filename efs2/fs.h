#ifndef FS_H_
#define FS_H_
#include "fsint.h"
#include "dev.h"
#include "cache.h"
#include "bitmap.h"
#include "inode.h"

struct efs_super_block {
  uint32_t s_magic;
  uint32_t s_inodes_count;
  uint32_t s_blocks_count;
  uint32_t s_free_inodes_count;
  uint32_t s_free_blocks_count;
  uint32_t s_first_data_block;
  uint32_t s_log_block_size;
  uint32_t s_first_ino;
};

struct efs_sb_info {
  struct efs_dev dev;
  struct efs_cache cache;
  struct efs_super_block *sb;
  struct efs_bitmap bm_inode;
  struct efs_bitmap bm_block;
  struct efs_inode_info inodes;
};

#define EFS_NAME_LEN 255

struct efs_dir_entry {
  uint32_t inode;
  __le16 rec_len;
  __u8 name_len;
  __u8 file_type;
  char name[EFS_NAME_LEN];
};

enum { EFS_FT_UNKNOWN = 0, EFS_FT_REG_FILE = 1, EFS_FT_DIR = 2 };

int efs_make(struct efs_dev *dev, size_t blk_size, size_t inodes,
             uint64_t img_size);
int efs_init(struct efs_sb_info *ctx, const char *fn);

#endif
