#ifndef DIR_H_
#define DIR_H_

#include "fs.h"
uint32_t efs_dir_locate(struct efs_sb_info *ctx, uint32_t inode_id,
                        const char *fn, struct efs_dir_entry *dir_entry);
uint32_t efs_dir_remove(struct efs_sb_info *ctx, uint32_t inode_id,
                        const char *fn, struct efs_dir_entry *dir_entry);
uint32_t efs_dir_remove_id(struct efs_sb_info *ctx, uint32_t inode_id,
                           uint32_t fn_inode_id,
                           struct efs_dir_entry *dir_entry);
uint32_t efs_dir_append(struct efs_sb_info *ctx, uint32_t dir_inode_id,
                        uint32_t fn_inode_id, const char *fn, __u8 file_type);
uint32_t efs_dir_resolve(struct efs_sb_info *ctx, uint32_t rel_inode_id,
                         uint32_t root_inode_id, char *path, char **fn);

#endif
