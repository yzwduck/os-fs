#include "fs.h"
#include "dev.h"
#include "inode.h"
#include "util.h"
#include "file.h"
#include "dir.h"

int main() {
  struct efs_sb_info ctx;
  struct efs_dev dev;
  int z = 16384;
  efs_dev_open(&dev, "efs.img");
  efs_make(&dev, 1024, z, 1024 * 1024 * 64);
  dev.close(&dev);
  return 0;
}
